package ru.inteview.crx.controller;

import ru.inteview.crx.service.RainyHillService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("/rainy")
public class RainyHillsController {

    @Inject
    private RainyHillService rainyHillService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public int hello(@QueryParam("hill") List<Integer> hills) {
        return rainyHillService.calculate(hills.stream().mapToInt(x -> x).toArray());
    }
}
