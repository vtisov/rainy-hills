package ru.inteview.crx.service;

import javax.ejb.Stateless;

@Stateless
public class RainyHillServiceInMemory implements RainyHillService {

    @Override
    public int calculate(int[] array) {

        if (array == null || array.length == 0) {
            return 0;
        }

        if (array.length > 32000) {
            throw new IllegalArgumentException("Max size of array is 32000");
        }

        final int indexOfMax = indexOfMax(array);
        int volume = 0;
        int leftLocalMax = 0;

        for (int i = 0; i < indexOfMax; i++) {

            int currentValue = array[i];

            if (currentValue < 0 && currentValue > 32000) {
                throw new IllegalArgumentException("Max size of hill is 32000");
            }

            leftLocalMax = Math.max(currentValue, leftLocalMax);

            if (currentValue < leftLocalMax) {
                volume += leftLocalMax - currentValue;
            }
        }

        int rightLocalMax = 0;

        for (int i = array.length - 1; i > indexOfMax; i--) {
            int currentValue = array[i];

            rightLocalMax = Math.max(currentValue, rightLocalMax);

            if (currentValue < rightLocalMax) {
                volume += rightLocalMax - currentValue;
            }
        }

        return volume;
    }

    private int indexOfMax(int[] array) {
        int indexOfMax = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > array[indexOfMax]) {
                indexOfMax = i;
            }
        }
        return indexOfMax;
    }

}
