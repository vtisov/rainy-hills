package ru.inteview.crx.controller;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.inteview.crx.RainyHillApplication;
import ru.inteview.crx.service.RainyHillService;
import ru.inteview.crx.service.RainyHillServiceInMemory;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.net.URI;
import java.net.URL;

import static junit.framework.TestCase.assertEquals;
import static org.jboss.shrinkwrap.api.ShrinkWrap.create;

@RunWith(Arquillian.class)
public class RainyHillsControllerTest {

    @ArquillianResource
    private URL base;

    private Client client;
    private String path = "api/rainy";

    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        return create(WebArchive.class)
                .addClasses(RainyHillApplication.class, RainyHillsController.class, RainyHillService.class, RainyHillServiceInMemory.class, JsonbBuilder.class, Jsonb.class);

    }

    @Before
    public void setup() {
        this.client = ClientBuilder.newClient();
    }

    @After
    public void teardown() {
        client.close();
    }

    @Test
    public void hello() throws Exception {
        WebTarget target = client.target(URI.create(new URL(base, path).toExternalForm()));
        Integer response = target.queryParam("hill", 4, 1, 0, 3).request().get(int.class);
        assertEquals(5, response.intValue());
    }

}