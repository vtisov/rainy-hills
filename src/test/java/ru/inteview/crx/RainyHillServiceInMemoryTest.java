package ru.inteview.crx;

import org.junit.Before;
import org.junit.Test;
import ru.inteview.crx.service.RainyHillService;
import ru.inteview.crx.service.RainyHillServiceInMemory;

import static junit.framework.TestCase.assertEquals;

public class RainyHillServiceInMemoryTest {

    private RainyHillService rainyHillService;

    @Before
    public void setUp() {
        rainyHillService = new RainyHillServiceInMemory();
    }

    @Test
    public void testCalculateWhenArrayIsNull() {
        int[] arrays = null;
        int volume = rainyHillService.calculate(arrays);
        assertEquals(0, volume);
    }

    @Test
    public void testCalculateWhenArrayIsEmpty() {
        int[] arrays = {};
        int volume = rainyHillService.calculate(arrays);
        assertEquals(0, volume);
    }


    @Test
    public void testCalculateWhenTwoEqualsMaxHill() throws Exception {
        int[] arrays = {2, 5, 1, 2, 5, 1};
        int volume = rainyHillService.calculate(arrays);
        assertEquals(7, volume);
    }

}